# PDFBox Signature API
##### API para firma digital

## Qué es PDFBox Signature API?
PDFBox Signature API es una librería que envuelve la implementación de referencia de PDFBox para facilitar la firma digital de docuentos PDF.

Los fuentes originales sobre los que está basada esta librería pueden encontrarse en el [Repositorio SVN Oficial de Apache](https://svn.apache.org/viewvc/pdfbox/trunk/examples/src/main/java/org/apache/pdfbox/examples/signature/) en los ejemplos para firma digital.

## Requisitos
- Java 11

## Instalación

```xml
<dependency>
    <groupId>com.somospnt</groupId>
    <artifactId>pdfbox-signature-api</artifactId>
    <version>2.0.0</version>    
</dependency>
```

## Uso de Librería
Ejemplo de uso con FileInputStream y FileOutputStream:

### Firmar PDF

```java
FileInputStream inputStream = new FileInputStream(INPUT_FILE);
FileOutputStream outputStream = new FileOutputStream(OUTPUT_FILE);
Signer signer = new Signer(KEYSTORE_PATH, PASSWORD, LOCATION, REASON);
signer.sign(inputStream, outputStream);
```

### Crear TimeStamp

```java
FileInputStream inputStream = new FileInputStream(INPUT_FILE);
FileOutputStream outputStream = new FileOutputStream(OUTPUT_FILE);
TimeStamper timeStamper = new TimeStamper(TSA_URL);
timeStamper.addTimeStamp(inputStream, outputStream);
```

### Agregar TimeStamp a firma ya existente

```java
FileInputStream inputStream = new FileInputStream(INPUT_FILE);
FileOutputStream outputStream = new FileOutputStream(OUTPUT_FILE);
TimeStamper timeStamper = new TimeStamper(TSA_URL);
timeStamper.embedTimeStamp(inputStream, outputStream);
```

## Licencia
PDFBox Signature API se distribuye bajo Apache Licence, version 2.0.
El archivo LICENSE contiene más información sobre la licencia de este producto.
Puede leer más sobre Apache Licence en [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0).