/*
 * Copyright 2015 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.somospnt.signature;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

/**
 * A simple wrapper class for signing PDF files using PDFBOX Example usage:
 *
 * <pre>
 * {@code
 * FileInputStream inputStream = new FileInputStream(INPUT_FILE);
 * FileOutputStream outputStream = new FileOutputStream(OUTPUT_FILE);
 * Signer signer = new Signer(KEYSTORE_PATH, PASSWORD, LOCATION, REASON);
 * signer.sign(inputStream, outputStream);
 * }
 * </pre>
 *
 * @author SomosPNT
 */
public class Signer {

    private final String certificatePath;
    private final String certificatePassword;
    private final String signatureLocation;
    private final String signatureReason;    

    /**
     * Initialize the signer with a path to the certificate and a password that
     * should be used for the signature.
     *
     * @param certificatePath is the path to the certificate location
     * @param certificatePassword is the pin for the certificate
     * @param signatureLocation is the location param of the signature
     * @param signatureReason is the reason param of the signature
     */
    public Signer(String certificatePath, String certificatePassword, String signatureLocation, String signatureReason) {
        this.certificatePath = certificatePath;
        this.certificatePassword = certificatePassword;
        this.signatureLocation = signatureLocation;
        this.signatureReason = signatureReason;        
    }

    Signer(String certificatePath, String certificatePassword, String signatureLocation, String signatureReason, String tsaUrl) {
        this.certificatePath = certificatePath;
        this.certificatePassword = certificatePassword;
        this.signatureLocation = signatureLocation;
        this.signatureReason = signatureReason;        
    }

    /**
     * Signs the given PDF file. Adds today date to the signature.
     *
     * @param inputStream the input stream that contains the PDF file
     * @param outputStream the output stream that contains the PDF signed file
     * @throws IOException if the file could not be read or signed
     */
    public void sign(InputStream inputStream, OutputStream outputStream) throws IOException {

        try {
            KeyStore keystore = KeyStore.getInstance("PKCS12");
            keystore.load(new FileInputStream(certificatePath), certificatePassword.toCharArray());

            CreateSignature signing = new CreateSignature(keystore, certificatePassword.toCharArray());            
            signing.setLocation(signatureLocation);
            signing.setReason(signatureReason);
            signing.signDetached(inputStream, outputStream);



        } catch (GeneralSecurityException ex) {
            throw new IOException("An error has occurred signing the document.", ex);
        }
    }

}
