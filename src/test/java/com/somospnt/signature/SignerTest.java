/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.somospnt.signature;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test for CreateSignature. Each test case will run twice: once with
 * SignatureInterface and once using external signature creation scenario.
 *
 * Utility class for the signature / timestamp examples.
 *
 * @author Tilman Hausherr
 * @author SomosPNT
 */
public class SignerTest {

    private static final String INPUT_DIR = "src/test/resources/";
    private static final String OUTPUT_DIR = "target/test-output/";
    private static final String KEYSTORE_PATH = INPUT_DIR + "TEST.pfx";
    private static final String PASSWORD = "123456";
    private static Certificate certificate;

    @BeforeClass
    public static void init() throws Exception {
        new File("target/test-output").mkdirs();

        KeyStore keystore = KeyStore.getInstance("PKCS12");
        keystore.load(new FileInputStream(KEYSTORE_PATH), PASSWORD.toCharArray());
        certificate = keystore.getCertificateChain(keystore.aliases().nextElement())[0];
    }

    /**
     * Signs a PDF using the "adbe.pkcs7.detached" SubFilter with the SHA-256
     * digest.
     *
     * @throws IOException
     * @throws GeneralSecurityException
     * @throws CMSException
     * @throws OperatorCreationException
     */
    @Test
    public void sign_signsFile() throws IOException, CMSException, OperatorCreationException, GeneralSecurityException {

        File inputFile = new File(INPUT_DIR + "input.pdf");
        File outputFile = new File(OUTPUT_DIR, "output_signed.pdf");

        Signer signer = new Signer(KEYSTORE_PATH, PASSWORD, "Location", "Reason");
        signer.sign(new FileInputStream(inputFile), new FileOutputStream(outputFile));

        checkSignature(new File(INPUT_DIR + "input.pdf"), new File(OUTPUT_DIR + "output_signed.pdf"));
    }

    private void checkSignature(File origFile, File signedFile)
            throws IOException, CMSException, OperatorCreationException, GeneralSecurityException {
        String origPageKey;
        try (PDDocument document = PDDocument.load(origFile)) {
            // get string representation of pages COSObject
            origPageKey = document.getDocumentCatalog().getCOSObject().getItem(COSName.PAGES).toString();
        }
        try (PDDocument document = PDDocument.load(signedFile)) {
            // PDFBOX-4261: check that object number stays the same
            Assert.assertEquals(origPageKey, document.getDocumentCatalog().getCOSObject().getItem(COSName.PAGES).toString());

            List<PDSignature> signatureDictionaries = document.getSignatureDictionaries();
            if (signatureDictionaries.isEmpty()) {
                Assert.fail("no signature found");
            }
            for (PDSignature sig : document.getSignatureDictionaries()) {
                COSString contents = (COSString) sig.getCOSObject().getDictionaryObject(COSName.CONTENTS);
                byte[] buf;
                try (FileInputStream fis = new FileInputStream(signedFile)) {
                    buf = sig.getSignedContent(fis);
                }
                // inspiration:
                // http://stackoverflow.com/a/26702631/535646
                // http://stackoverflow.com/a/9261365/535646
                CMSSignedData signedData = new CMSSignedData(new CMSProcessableByteArray(buf), contents.getBytes());
                Store<X509CertificateHolder> certificatesStore = signedData.getCertificates();
                Collection<SignerInformation> signers = signedData.getSignerInfos().getSigners();
                SignerInformation signerInformation = signers.iterator().next();
                @SuppressWarnings("unchecked")
                Collection matches = certificatesStore.getMatches((Selector<X509CertificateHolder>) signerInformation.getSID());
                X509CertificateHolder certificateHolder = (X509CertificateHolder) matches.iterator().next();
                X509Certificate certFromSignedData = new JcaX509CertificateConverter().getCertificate(certificateHolder);
                Assert.assertEquals(certificate, certFromSignedData);
                // CMSVerifierCertificateNotValidException means that the keystore wasn't valid at signing time
                if (!signerInformation.verify(new JcaSimpleSignerInfoVerifierBuilder().build(certFromSignedData))) {
                    Assert.fail("Signature verification failed");
                }
                break;
            }
        }
    }

}
