/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.somospnt.signature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.junit.Test;

/**
 * Test for CreateSignature. Each test case will run twice: once with
 * SignatureInterface and once using external signature creation scenario.
 *
 * Utility class for the signature / timestamp examples.
 *
 * @author Tilman Hausherr
 * @author SomosPNT
 */
public class TimeStamperTest {

    private static final String INPUT_DIR = "src/test/resources/";
    private static final String OUTPUT_DIR = "target/test-output/";

    @Test
    public void addTimeStamp_addsTimeStampToFile() throws IOException, CMSException, OperatorCreationException, GeneralSecurityException {

        File inputFile = new File(INPUT_DIR + "input.pdf");
        File outputFile = new File(OUTPUT_DIR, "output_timeStamped.pdf");
        String tsaUrl = "http://sha256timestamp.ws.symantec.com/sha256/timestamp";

        TimeStamper timeStamper = new TimeStamper(tsaUrl);
        timeStamper.addTimeStamp(new FileInputStream(inputFile), new FileOutputStream(outputFile));
    }

    @Test
    public void embedTimeStamp_addsTimeStampToPreviousSignedPdf() throws IOException {

        File inputFile = new File(INPUT_DIR + "input_signed.pdf");
        File outputFile = new File(OUTPUT_DIR, "output_embedTimeStamp.pdf");
        String tsaUrl = "http://sha256timestamp.ws.symantec.com/sha256/timestamp";

        TimeStamper timeStamper = new TimeStamper(tsaUrl);
        timeStamper.embedTimeStamp(new FileInputStream(inputFile), new FileOutputStream(outputFile));
    }

    @Test(expected = IOException.class)
    public void embedTimeStamp_withNotSignedPdf_throwsIOException() throws IOException {

        File inputFile = new File(INPUT_DIR + "input.pdf");
        File outputFile = new File(OUTPUT_DIR, "output_embedTimeStamp.pdf");
        String tsaUrl = "http://sha256timestamp.ws.symantec.com/sha256/timestamp";

        TimeStamper timeStamper = new TimeStamper(tsaUrl);
        timeStamper.embedTimeStamp(new FileInputStream(inputFile), new FileOutputStream(outputFile));
    }

}
